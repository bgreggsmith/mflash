program mflash;

{$Mode ObjFpc}

uses
	crt,
	sysutils,
	synaser;

const
	RType_Data		=	0;
	RType_EOF		=	1;
	RType_ExtSeg	=	2;

var
	b: char;
	sourcePath, portPath, lineIn, buffer: ANSIString;
	serCon: TBlockSerial;
	lineCounter: Int64;
	i, z: LongWord;
	Disk: Text;
	recSize, recAddrHigh, recAddrLow, recType: Byte;
	nRecs: Int64;
	flag_skipEntryCheck: Boolean;
	offset: Int64;

procedure SlowTX(Data: ANSIString);
var
	i: LongWord;
	bChk: Byte;
	
begin
	for i := 1 to Length(Data) do
		begin
			serCon.SendString(buffer[i]);
			
			bChk := serCon.recvbyte(-1)
			//write(IntToHex(bChk, 2),' ');
		end;
	//writeln('');
end;

function byteValues(bin: ANSIString): ANSIString;
var
	i: LongWord;
	
begin
	byteValues := '';
	
	for i := 1 to Length(bin) do
		byteValues += IntToHex(Ord(bin[i]), 2) + ' ';
end;

function hexPairToByte(Pair: ANSIString): Byte;
var
	hex1, hex2: Byte;
	
begin
	if (Ord(Pair[1]) >= Ord('0')) and (Ord(Pair[1]) <= Ord('9')) then
		hex1 := Ord(Pair[1]) - Ord('0');
	
	if (Ord(Pair[2]) >= Ord('0')) and (Ord(Pair[2]) <= Ord('9')) then
		hex2 := Ord(Pair[2]) - Ord('0');
	
	if (Ord(Pair[1]) >= Ord('A')) and (Ord(Pair[1]) <= Ord('F')) then
		hex1 := 10 + Ord(Pair[1]) - Ord('A');
	
	if (Ord(Pair[2]) >= Ord('A')) and (Ord(Pair[2]) <= Ord('F')) then
		hex2 := 10 + Ord(Pair[2]) - Ord('A');
	
	hexPairToByte := (hex1 * 16) + hex2;
end;
	
begin
	if ParamCount() <= 1 then
		begin
			writeln('Usage: mflash [hex] [port]');
			writeln('		mflash firmware.hex /dev/ttyUSB0');
			Halt;
		end;
	
	sourcePath := ParamStr(1);
	portPath := ParamStr(2);
	
	write('Connect to serial port [',portPath,']...');
	serCon := TBlockSerial.Create();
	serCon.Connect(portPath);
	serCon.Config(115200, 8, 'N', SB1, false, false);
	writeln(' ok');
	
	//Wait for the boot message of MErcury
	write('Waiting for device connection...');
	repeat
		lineIn := serCon.RecvString(10);
		until Pos('MErcury', lineIn) > 0;
	
	
	writeln(' ok');
	//Immediately reply with fwupd message to enter flashing mode on MErcury device
	serCon.SendString('fwupd' + #10);
	
	write('Waiting for device ready...');
	//Wait for the device to be ready to accept firmware
	repeat
		lineIn := serCon.RecvString(10);
		until Pos('ready', lineIn) > 0;
	writeln(' ok');
	
	writeln('Uploading...');
	writeln('Upload messages:');
	
	Assign(Disk, sourcePath);
	Reset(Disk);
	
	nRecs := 0;
	repeat
		readln(Disk, lineIn);
		nRecs += 1;
		until eof(Disk);
	
	Reset(Disk);
	
	offset := 0;
	flag_skipEntryCheck := False;
	lineCounter := 0;
	repeat
		//When the device is ready for a frame, it sends us a '>'
		if flag_skipEntryCheck = False then
			begin
				b := #0;
				while b <> '>' do
					begin
						b := char(serCon.recvbyte(-1));
						//write(b);
					end;
			end;
			
		readln(Disk, lineIn);
		lineCounter += 1;
		
		if lineIn[1] <> ':' then
			begin
				writeln(sourcePath,':',lineCounter,' invalid start of record character!');
				continue;
			end;
		
		recSize := hexPairToByte(lineIn[2] + lineIn[3]);
		recAddrHigh := hexPairToByte(lineIn[4] + lineIn[5]);
		recAddrLow := hexPairToByte(lineIn[6] + lineIn[7]);
		recType := hexPairToByte(lineIn[8] + lineIn[9]);
		
		if (recSize <= 0) and (recType = RType_Data) then
			begin
				writeln(sourcePath,':',lineCounter,' invalid record data size!');
				continue;
			end;
		
		case recType of
				RType_Data: //Data record, formulate a program packet to send to device
					begin
						buffer := Char(recSize) + Char(recType) + Char(recAddrHigh) + Char(recAddrLow);
						
						//we really should do some form of CRC
						i := 0;
						z := 10;
						repeat
							buffer += Char(hexPairToByte(lineIn[z] + lineIn[z+1]));
							i += 1;
							z += 2;
							until i >= recSize;
						
						buffer := Char(Length(buffer)) + buffer;
						
						//writeln('pSize=',recSize,' addr=',(recAddrHigh * 256) + recAddrLow,' ', byteValues(buffer));
						
						//GoToXY(1, WhereY());
						writeln((lineCounter * 100) div nRecs,'% ',IntToHex((recAddrHigh * 256) + recAddrLow, 4));
						
						SlowTX(buffer);
						
						//writeln('recSize = ',recSize,' recAddr=',(recAddrHigh * 256) + recAddrLow, ' encap=',Length(buffer),' b0=',Ord(Buffer[1]));
						//writeln('dump[',byteValues(buffer),']');
						//readln();
					end;
				RType_EOF: //End of file, send finalisation message to device
					begin
						buffer := Char(2) + Char(0) + Char(recType);
						
						writeln((lineCounter * 100) div nRecs,'% ',IntToHex((recAddrHigh * 256) + recAddrLow, 4));
						
						SlowTX(buffer);
					end;
			else
				begin
					writeln(sourcePath,':',lineCounter,' unknown record type ',recType);
					flag_skipEntryCheck := True;
				end;
			end;
		
		//delay(5000);
		until eof(Disk);
	
	Close(Disk);
	
	writeln('Upload completed');
	
	serCon.Purge();
	serCon.CloseSocket();
	serCon.Destroy();
end.
